-- VHDL Test Bench Created by ISE for module: WebPack_QuickStart
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
USE ieee.numeric_std.ALL;
 
ENTITY WebPack_QuickStart_tb IS
END WebPack_QuickStart_tb;
 
ARCHITECTURE behavior OF WebPack_QuickStart_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT WebPack_QuickStart
    PORT(
         WA : OUT  std_logic_vector(15 downto 0);
         WB : OUT  std_logic_vector(15 downto 0);
         WC : OUT  std_logic_vector(15 downto 0);
         clk : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';

 	--Outputs
   signal WA : std_logic_vector(15 downto 0);
   signal WB : std_logic_vector(15 downto 0);
   signal WC : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 31.25 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: WebPack_QuickStart PORT MAP (
          WA => WA,
          WB => WB,
          WC => WC,
          clk => clk
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
--stop_simulation :process
--begin
--	wait for 100 ns; --run the simulation for this duration
--	assert false
--		report "simulation ended"
--		severity note;
--	wait;
--end process ;


   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clk_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
